#!/usr/bin/env bash

echo "# Progress Screenshots"
echo
echo "| Commit | Screenshot |"
echo "|--------|------------|"
i=$(git rev-list HEAD | wc -l)
for f in *.png
do
    i=$((i - 1))
    echo "| $(git rev-parse HEAD~$i) $(git show HEAD~$i --pretty=format:%s --no-patch) | ![$i]($f) |"
done
