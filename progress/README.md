# Progress Screenshots

| Commit | Screenshot |
|--------|------------|
| a8180b169a7d89b84dd207c82d030cbbe8d218a6 Initial commit | ![67](screencapture-localhost-8000-2023-09-24-18_47_23.png) |
| c543a1129de0f57904ed33566292a7a328b2a411 Add field | ![66](screencapture-localhost-8000-2023-09-24-18_57_05.png) |
| 67a4993233068f5e977db38bec50ec9505ead0b1 Initial car movement | ![65](screencapture-localhost-8000-2023-09-24-19_10_09.png) |
| 64445787aaa22fb209e318d90eb05908a545ae38 Better car simluation | ![64](screencapture-localhost-8000-2023-09-24-19_46_24.png) |
| d47d62f2aaf367a3e7661eb57066987bf12b6b69 Add EXPLOSION | ![63](screencapture-localhost-8000-2023-09-24-19_51_12.png) |
| 7ce5ca38df418238b3375d2f5546e06764e3e73e Implement turning right | ![62](screencapture-localhost-8000-2023-12-03-19_47_17.png) |
| c569dbc33fc2aa2ec1e3c9e2b5b7a24cb8478672 Implement turning left | ![61](screencapture-localhost-8000-2023-12-03-19_59_21.png) |
| e8e81bba9e994e908a62ce27a96f5f7d77611eb7 Rotate car sprite | ![60](screencapture-localhost-8000-2023-12-03-20_10_29.png) |
| e209960d3469acfd359e4eb412bcf5bc790314b7 Add car sprite | ![59](screencapture-localhost-8000-2023-12-03-20_40_30.png) |
| 4ab96b5d87452dd2261e5eb3d315225bc761d43b Change order of frames and animation speed of explosion.gif | ![58](screencapture-localhost-8000-2023-12-03-21_06_28.png) |
| 30ae7caf1e730ecd713b8be65870119c02402a81 Add grass-green background | ![57](screencapture-localhost-8000-2023-12-04-17_33_44.png) |
| e5412d8988e2fbe7e02edb28fa62676034882fde Add simple race track | ![56](screencapture-localhost-8000-2023-12-04-17_49_11.png) |
| 1d74ca867b0a25430cc42106839556fdf4a37f26 Add white lines on the road | ![55](screencapture-localhost-8000-2023-12-04-17_54_43.png) |
| 7bbb4072ee8eb2916dea4f17a5184fa1ab19177c Add red-white borders around the track | ![54](screencapture-localhost-8000-2023-12-04-18_03_09.png) |
| 15afe1833983b12b8c49954e017561652721a361 Increase game speed so that command duration matches seconds again, instead of half seconds | ![53](screencapture-localhost-8000-2023-12-04-19_04_42.png) |
| eb1a7e7fdfbf68048e22f14319b964cad79b7595 Implement SVG-path generation from commands | ![52](screencapture-localhost-8000-2023-12-05-20_18_13.png) |
| 2ee866ed8f5902df39afd385782d48b91b266b1a Make road stay centered on car | ![51](screencapture-localhost-8000-2023-12-05-20_31_47.png) |
| 3e7f50b41da6c3ad85f266fe972d3ed02f7a7b6d Implement way to change between levels via hash in URL | ![50](screencapture-localhost-8000-2023-12-07-13_22_59.png) |
| ed49cc7e3f30fa6edcb5ec072a9d5b7aca30db30 Load correct level index on page load | ![49](screencapture-localhost-8000-2023-12-08-14_26_50.png) |
| 5c34f8d738264ae2d1d2f414d4b4ff2fd2a3942b Clean up explosion from previous level, if any | ![48](screencapture-localhost-8000-2023-12-08-14_33_11.png) |
| 4df9c0d2cc6ccb5a1d647d6c3a1d73c42b301a2d Extract car AI per level, instead of globally | ![47](screencapture-localhost-8000-2023-12-08-14_40_56.png) |
| dcc7fdafc1f5f493694094a1720657d8a8a92aa8 Add start/finish line | ![46](screencapture-localhost-8000-2023-12-08-15_02_01.png) |
| e9ce2233b1b139ce34138030b137e395e63019b1 Extract Road and Car classes | ![45](screencapture-localhost-8000-2023-12-08-15_41_46.png) |
| bb503cbff5d5bd0fb4ff039ac8a553e826702e38 Implement collision-checking: when car moves outside road, it goes boom | ![44](screencapture-localhost-8000-2023-12-08-18_22_20.png) |
| c098f06e70bc93844a6ccde8db355cae4039e771 Add a PlayerCar that can be controlled using the keyboard | ![43](screencapture-localhost-8000-2023-12-08-20_07_34.png) |
| 7c099302575220a5f71f891f4ca4985c8e996138 Allow driving backwards | ![42](screencapture-localhost-8000-2023-12-08-20_40_08.png) |
| be32bc6a9d584537025be2a660e006afb6650f72 Add game time in fancy font | ![41](screencapture-localhost-8000-2023-12-08-21_23_22.png) |
| 595da0c2e6b9cb36e3f3a1b0cb5d49a92a7c9322 Add speedometer | ![40](screencapture-localhost-8000-2023-12-08-22_15_39.png) |
| 5ee6dfbcb38da7d0ba88689935d0e66ad28ef64b Add arcade background image | ![39](screencapture-localhost-8000-2023-12-08-22_28_10.png) |
| 4ca8d95bcebb387d554dec045e6613c4b6143219 Add customizable cars | ![38](screencapture-localhost-8000-2023-12-09-09_01_13.png) |
| 690cac27005682e1606f3f0b036f61ab131a382f Add Minecraft fire after explosion | ![37](screencapture-localhost-8000-2023-12-09-09_51_06.png) |
| 8001e8a1e8eefe817e4d95baf41559d56239ac56 Add explosion to AI cars | ![36](screencapture-localhost-8000-2023-12-10-21_29_32.png) |
| cd42a2408e4589d5a0b71282ac9cbb63647986e0 Detect when car crosses finish line | ![35](screencapture-localhost-8000-2023-12-12-19_16_50.png) |
| a028a6d098ee2c10ee7c0845bde6026e2fb8f9e5 Add CRT monitor effects | ![34](screencapture-localhost-8000-2023-12-12-20_05_38.png) |
| cc0dea8a042def053d385ecfee142cface25ab7c Add menu screen | ![33](screencapture-localhost-8000-2023-12-12-20_51_39.png) |
| d922c91090ca46e98620068a3701844bd87050f6 Add photos of Maarten and Menno with face removed | ![32](screencapture-localhost-8000-2023-12-16-18_28_56.png) |
| e00a80a6efe340019aa5ef1126590440e33d40b9 Add emoji behind empty faces | ![31](screencapture-localhost-8000-2023-12-16-18_29_41.png) |
| a2973653017e4c93f9aa26707005d214cc84bb64 Scale Maarten's emoji in the vertical direction | ![30](screencapture-localhost-8000-2023-12-16-18_40_40.png) |
| 2385b492b7603060931e2e44ad0f3f02b64fc449 Add text balloons to intro | ![29](screencapture-localhost-8000-2023-12-16-20_10_23.png) |
| 44e4a1d9e319f0aaec6059b019a7d5ab9ccb65dd Add animations to intro screen | ![28](screencapture-localhost-8000-2023-12-16-21_20_00.png) |
| 8556453951d5b4af32cde0256249f29e884565b0 Correctly turn monitor on and off when switching Controller | ![27](screencapture-localhost-8000-2023-12-16-21_39_06.png) |
| 687cf55199832b54e8c8dbb73b4a55bc0f595be5 Add intro music | ![26](screencapture-localhost-8000-2023-12-18-21_52_26.png) |
| 9d43e6ffed5489f0543be1649cff8daa8d8b8dcd Actually mute the music when clicking the music button | ![25](screencapture-localhost-8000-2023-12-19-19_54_25.png) |
| bd545e97a4c64b9ac89aad43e9e4e5662612bbef Add menu and level music | ![24](screencapture-localhost-8000-2023-12-19-20_27_44.png) |
| 037c7a2824b77ccd1b2873c74feefa8bb5008159 Prevent sound loading issues by forcing user interaction before loading the game | ![23](screencapture-localhost-8000-2023-12-20-19_48_34.png) |
| 91ec623d53d5794ee5f067a9186e2badbfa3c4b3 Play vrooming sound for car | ![22](screencapture-localhost-8000-2023-12-20-20_19_44.png) |
| 15320cc76c7fc74ec639d6e97d2073f9a9ca6b12 Implement muting/unmuting the vrooming sound | ![21](screencapture-localhost-8000-2023-12-20-20_31_57.png) |
| 92d34d9d889ad2af6d4698bba2617ec2c525a9ed Add explosion sound | ![20](screencapture-localhost-8000-2023-12-20-20_51_42.png) |
| d6d524a9dc35324f08fd7c120d82b29d87254e62 Set sound volume based on distance to screen | ![19](screencapture-localhost-8000-2023-12-20-21_07_31.png) |
| 9b313715d0a03baf5a60747a33962fdf759ff3c6 Add vroom sounds to AI cars | ![18](screencapture-localhost-8000-2023-12-20-21_33_56.png) |
| c7e58dff1998c7328c826c7c99ddd8a985306b4b Implement doppler effect for vroom sound | ![17](screencapture-localhost-8000-2023-12-22-20_49_42.png) |
| c6e9f27ef25a20b0019fb1e1ce1f52bd085d8d57 Add countdown numbers and sound when starting level | ![16](screencapture-localhost-8000-2023-12-22-22_30_26.png) |
| 71ec072d7b1a8b4849dfb4e4d7e5cd3a9ec05ba3 Add ?instaStart parameter to URL to skip level start sequence | ![15](screencapture-localhost-8000-2023-12-23-19_56_51.png) |
| 9263f17cb9008ef1c1013de623c29f51888a3f84 Wait with playing level music until start sequence finished | ![14](screencapture-localhost-8000-2023-12-23-20_19_48.png) |
| 86e6f8046f0514146a4e5b2ea20a71b6a79d4bf0 Show best lap time | ![13](screencapture-localhost-8000-2023-12-23-21_01_41.png) |
| 3de2ddaf4047e5020d2689ccec4efb5c7a46bdcf Add conversation between levels | ![12](screencapture-localhost-8000-2023-12-23-22_22_08.png) |
| ced432980f631ea7432820dced0bc07fcf743f35 Correctly reset/destroy introHandler when switching levels | ![11](screencapture-localhost-8000-2023-12-24-12_05_43.png) |
| dcedd0d3051a8aca9a6239e08c3f006062b91200 Immediately apply any keys that player has pressed when start sequence ends | ![10](screencapture-localhost-8000-2023-12-24-12_07_42.png) |
| 16b2893719a7673ce09b2fe7b366da6f4deb99ed Add victory screen with lap times | ![9](screencapture-localhost-8000-2023-12-24-13_31_06.png) |
| d35face9d5710f174946cc027134bfa660851571 Add "Press R to restart" | ![8](screencapture-localhost-8000-2023-12-24-14_01_19.png) |
| 95c68f2cdd3fb1fdc504b79704c54b43a50dadfb Add beep sounds | ![7](screencapture-localhost-8000-2023-12-25-08_10_27.png) |
| e63d33c01d9a71274ac5e0a52e53fb555d4618fe Allow changing zoom level with '0', '-', and '=' | ![6](screencapture-localhost-8000-2023-12-25-08_24_18.png) |
| 02503684120df00478f9a29d465f024d49edd19f Add Happy Birthday level | ![5](screencapture-localhost-8000-2023-12-25-10_57_27.png) |
| 5c52e4907230bd3992696091fe45d593b9e33d48 Fix some small visual bugs | ![4](screencapture-localhost-8000-2023-12-25-12_03_47.png) |
| 3e77a8431715545592b8c52a1234dc4f0a0ba489 Add victory sound, rebalance level music volume | ![3](screencapture-localhost-8000-2023-12-25-12_15_19.png) |
| 29ef0ae647d3910570c12e0962baaa796a8806c9 Add outro | ![2](screencapture-localhost-8000-2023-12-25-12_38_02.png) |
| 2a60e17bdf310bb06c56c0f87e539dfa50219b81 Allow speeding up/down while turning | ![1](screencapture-localhost-8000-2023-12-25-13_15_06.png) |
