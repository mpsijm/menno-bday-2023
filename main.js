const load = () => {
    const ACC = 128;
    const pi = Math.PI;
    const twoPi = 2 * pi;
    const halfPi = pi / 2;
    const quartPi = pi / 4;

    const beeps = ["daan", "davina", "jelle", "lisette", "maarten", "marie", "tom"];

    const $monitor = document.getElementById("monitor");
    const $menu = document.getElementById("menu");
    const $field = document.getElementById("field");

    const $map = document.getElementById("map");
    const $road = document.getElementById("road");
    const $time = document.getElementById("time");
    const $bestLap = document.getElementById("best-lap");
    const $speed = document.getElementById("speed");
    const $speedometer = document.getElementById("speedometer");
    const $victory = document.getElementById("victory");
    const $restart = document.getElementById("restart");

    const instaStart = location.search === "?instaStart";

    const dist = (dx, dy) => Math.sqrt(dx * dx + dy * dy);

    const isSingleKey = (e) => !e.ctrlKey && !e.shiftKey && !e.altKey && !e.metaKey &&
        (e.key.length === 1 || isSpaceKey(e));

    const isSpaceKey = (e) => !e.ctrlKey && !e.shiftKey && !e.altKey && !e.metaKey &&
        [" " , "Tab", "Enter"].includes(e.key);

    const printTime = (t) => Math.floor(t / 60).toString().padStart(2, "0") + ":" + (t % 60).toFixed(3).padStart(6, "0");

    const wait = millis => new Promise(resolve => setTimeout(resolve, millis));

    const parseCommands = (commandStr) => {
        const strs = commandStr.trim().split(/\s+/);
        if (strs.length % 2 != 0) throw new Error("Invalid commandStr: number of elements is odd");
        const commands = [];
        for (let i = 0; i < strs.length; i += 2) {
            if (!("adilr".includes(strs[i]) || strs[i].length == 2 && "ad".includes(strs[i][0]) && "lr".includes(strs[i][1])))
                throw new Error(`Invalid commandStr: unknown command ${strs[i]}`);
            const d = parseFloat(strs[i + 1]);
            if (isNaN(d)) throw new Error(`Invalid commandStr: invalid duration ${strs[i + 1]}`);
            commands.push([strs[i], d]);
        }
        return commands;
    };

    const delta = {
        "a": (t, r, sx, sy) => [
            .5 * ACC * t * t * Math.cos(r) + sx * t,
            .5 * ACC * t * t * Math.sin(r) + sy * t,
            r,
            sx + ACC * t * Math.cos(r),
            sy + ACC * t * Math.sin(r),
        ],
        "d": (t, r, sx, sy) => [
            -.5 * ACC * t * t * Math.cos(r) + sx * t,
            -.5 * ACC * t * t * Math.sin(r) + sy * t,
            r,
            sx - ACC * t * Math.cos(r),
            sy - ACC * t * Math.sin(r),
        ],
        "i": (t, r, sx, sy) => [
            sx * t,
            sy * t,
            r,
            sx,
            sy,
        ],
        // Assumption: traversing a full circle (2π rad) takes π seconds
        // Therefore, we multiply the angular velocity with 2 and divide the radius by 2
        "r": (t, r, sx, sy) => {
            const s = dist(sx, sy);
            const oldR = r;
            r = Math.atan2(sy, sx);
            const newR = r + t * 2;
            return [
                (-s * Math.sin(r) + s * Math.sin(newR)) / 2,
                ( s * Math.cos(r) - s * Math.cos(newR)) / 2,
                oldR + t * 2,
                s * Math.cos(newR),
                s * Math.sin(newR),
            ];
        },
        "l": (t, r, sx, sy) => {
            const s = dist(sx, sy);
            const oldR = r;
            r = Math.atan2(sy, sx);
            const newR = r - t * 2;
            return [
                ( s * Math.sin(r) - s * Math.sin(newR)) / 2,
                (-s * Math.cos(r) + s * Math.cos(newR)) / 2,
                oldR - t * 2,
                s * Math.cos(newR),
                s * Math.sin(newR),
            ];
        },
    };

    const processDelta = (c, t, r, sx, sy) => {
        if (c.length === 1) {
            return delta[c](t, r, sx, sy);
        } else {
            const [x1, y1, r1, sx1, sy1] = delta[c[0]](t, r, sx, sy);
            return delta[c[1]](t, r1, sx1, sy1);
        }
    };

    class Segment {
        constructor(x1, y1, x2, y2) {
            [this.x1, this.y1, this.x2, this.y2] = [x1, y1, x2, y2];
            this.dx = x2 - x1;
            this.dy = y2 - y1;
        }
    }

    class LineSegment extends Segment {
        constructor(x1, y1, x2, y2) {
            super(x1, y1, x2, y2);
            this.length = dist(this.dx, this.dy);
        }

        // Source: https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_two_points
        distToPoint(x, y) {
            const {x1, y1, x2, y2} = this;
            return Math.abs((x2-x1) * (y1-y) - (x1-x) * (y2-y1)) / this.length;
        }

        // Source: https://en.wikipedia.org/wiki/Angle#Dot_product_and_generalisations
        // Rewritten to: θ = acos(u·v / (|u||v|))
        angleToPoint(x, y) {
            const other = new LineSegment(this.x1, this.y1, x, y);
            // For some reason, the result of (u·v / (|u||v|)) sometimes ends up being ever-so-slightly larger than 1.0...
            return Math.acos(Math.max(-1, Math.min(1,
                (this.dx * other.dx + this.dy * other.dy) / this.length / other.length)));
        }

        reversed() {
            return new LineSegment(this.x2, this.y2, this.x1, this.y1);
        }

        svgPath() {
            return `l ${this.dx},${this.dy}`;
        }

        // Giving one pixel more leeway in every direction
        contains(x, y) {
            const d = this.distToPoint(x, y);
            const a1 = this.angleToPoint(x + this.dx / this.length, y + this.dy / this.length);
            const a2 = this.reversed().angleToPoint(x - this.dx / this.length, y - this.dy / this.length);
            return d < 81 && a1 < halfPi && a2 < halfPi;
        }

        static angleBetweenVectors(x1, y1, x2, y2) {
            return new LineSegment(0, 0, x1, y1).angleToPoint(x2, y2);
        }
    }

    class CircleSegment extends Segment {
        constructor(cx, cy, x1, y1, x2, y2, c) {
            super(x1, y1, x2, y2);
            [this.cx, this.cy] = [cx, cy];

            const [r1, a1] = this.radiusAndAngle(x1, y1);
            const [r2, a2] = this.radiusAndAngle(x2, y2);
            if (Math.abs(r1 - r2) > 1e-6)
                throw new Error("CircleSegment has no constant radius");
            this.radius = r1;

            // Note that a1 and a2 are swapped with respect to xy1 and xy2 when a2 < a1.
            // This helps in this.contains().
            // Arc length (theta, in radians) calculated using https://stackoverflow.com/a/63890692.
            // The same comment as LineSegment.angleToPoint applies here.
            const theta = Math.acos(Math.max(-1, Math.min(1,
                1 - (Math.pow(dist(this.dx, this.dy), 2) / (2 * Math.pow(r1, 2))))));
            [this.a1, this.a2] = c === "r" ? [a1, a1 + theta] : a1 - theta < -halfPi ? [a1 + twoPi - theta, a1 + twoPi] : [a1 - theta, a1];

            this.length = this.radius * (this.a2 - this.a1);
        }

        radiusAndAngle(x, y) {
            return [
                dist(y - this.cy, x - this.cx),
                Math.atan2(y - this.cy, x - this.cx),
            ];
        }

        svgPath(reverse) {
            return `a ${this.radius},${this.radius} 0 0 ${reverse ? 1 : 0} ${this.dx},${this.dy}`;
        }

        // Giving one pixel more leeway in every direction
        contains(x, y) {
            const [radius, angle] = this.radiusAndAngle(x, y);
            const a = angle < this.a1 ? angle + twoPi : angle;
            const dr = Math.abs(this.radius - radius);
            return dr < 81 && this.a1 - 1 / this.radius < a && a < this.a2 + 1 / this.radius;
        }
    }

    class PathTraverser {
        anchors;
        commands;

        constructor(startSx, commands) {
            // Anchor order: [t, x, y, r, sx, sy]
            this.anchors = [[0, 0, 0, 0, startSx, 0]];
            this.commands = parseCommands(commands);
            this.commands.forEach(command => this.addAnchorFromCommand(command));
        }

        // [command, duration]
        addAnchorFromCommand([c, d]) {
            const [at, ax, ay, ar, asx, asy] = this.anchors.at(-1);
            const [x, y, r, sx, sy] = processDelta(c, d, ar, asx, asy);
            this.anchors.push([at + d, ax + x, ay + y, r, sx, sy]);
        }
    }

    class Road extends PathTraverser {
        length = 0;
        segments = [];
        svgPath;

        constructor(startSx, commands) {
            super(startSx, commands);

            this.commands.forEach(([c, d], i) => {
                const [x1, y1, r1] = this.anchors[i].slice(1, 4);
                const [x2, y2] = this.anchors[i + 1].slice(1, 3);
                if (c == "l" || c == "r") {
                    const [sx, sy] = this.anchors[i + 1].slice(4, 6);
                    const radius = dist(sx, sy) / 2;
                    const [cx, cy] = c == "r"
                        ? [-radius * Math.sin(r1),  radius * Math.cos(r1)]
                        : [ radius * Math.sin(r1), -radius * Math.cos(r1)];
                    this.segments.push(new CircleSegment(x1 + cx, y1 + cy, x1, y1, x2, y2, c));
                } else {
                    this.segments.push(new LineSegment(x1, y1, x2, y2));
                }
            });

            this.length = this.segments.map(s => s.length).reduce((a, b) => a + b, 0);
            this.svgPath = "M 0 0 " + this.segments.map((s, i) => s.svgPath(this.commands[i][0] == "r")).join(" ");
        }
    }

    class Car extends PathTraverser {
        id;
        maxT;
        $car;
        vroom;

        x = 0;
        y = 0;
        r = 0;
        sx = 0;
        sy = 0;
        segmentIndex = 0;
        laps = 0;
        exploded = false;

        constructor(commands, id = "") {
            super(0, commands);

            this.id = id;
            this.maxT = this.anchors.at(-2)[0];
            this.$car = document.getElementById("car" + id);

            this.vroom = new Sound("vroom", id);
            this.vroom.play();
        }

        commandIndex(t) {
            if (t >= this.maxT)
                return this.commands.length - 1;
            for (let i = this.commands.length - 1; i >= 0; i--) {
                const [at, ax, ay, ar, sx, sy] = this.anchors[i];
                if (t >= at) {
                    return i;
                }
            }
            throw new Error("Unreachable");
        };

        step(t, road, playerCar) {
            if (this.exploded) {
                return;
            }

            const i = this.commandIndex(t);
            const [at, ax, ay, ar, asx, asy] = this.anchors[i];
            [this.x, this.y, this.r, this.sx, this.sy] = processDelta(this.commands[i][0], t - at, ar, asx, asy);
            this.x += ax;
            this.y += ay;
            this.s = dist(this.sx, this.sy);

            // Scalar projection of speed based on https://en.wikipedia.org/wiki/Scalar_projection
            // Doppler factor calculated based on https://en.wikipedia.org/wiki/Doppler_effect
            // Note that `playerCar` is the receiver of the sound, and `this` is the source of the sound.
            const theta_r = LineSegment.angleBetweenVectors(this.x - playerCar.x, this.y - playerCar.y, playerCar.sx, playerCar.sy);
            const theta_s = LineSegment.angleBetweenVectors(playerCar.x - this.x, playerCar.y - this.y, this.sx, this.sy);
            const v_r = (playerCar.s === 0 ? 0 : Math.cos(theta_r) * playerCar.s);
            const v_s = (this.s === 0 ? 0 : Math.cos(theta_s) * this.s);
            const dopplerFactor = this.x === playerCar.x && this.y === playerCar.y ? 1 : (2048 + v_r) / (2048 - v_s);
            this.vroom.audioElement.playbackRate = dopplerFactor * (1 + this.s / 1000);

            const distanceToScreen = dist(playerCar.x - this.x, playerCar.y - this.y);
            this.vroom.setVolume(distanceToScreen, dopplerFactor);

            const n = road.segments.length;
            if (road.segments.at(this.segmentIndex).contains(this.x, this.y)) {
                return;
            }
            for (let d = 1; d < n / 2; d++) {
                for (let dir = -1; dir <= 1; dir += 2) {
                    const i = (this.segmentIndex + d * dir + n) % n;
                    if (road.segments.at(i).contains(this.x, this.y)) {
                        if (this.segmentIndex < n / 2 && i > n / 2) {
                            this.laps -= 1;
                        }
                        if (this.segmentIndex > n / 2 && i < n / 2) {
                            this.laps += 1;
                        }
                        this.segmentIndex = i % n;
                        return;
                    }
                }
            }
            this.explode(distanceToScreen);
        };

        draw() {
            if (this.exploded) {
                return;
            }

            const {x, y, r} = this;
            this.$car.style.transform = `translate(${x}px,${y}px)`;
            this.$car.children[0].style.transform = `rotate(${r * 180 / pi}deg) translate(-32px,-16px)`;
        }

        explode(distanceToScreen) {
            this.exploded = true;

            this.vroom.stop();
            soundManager.play(`explosion${Math.floor(Math.random() * 4)}`, this.id, distanceToScreen);

            const $explosion = document.createElement("div");
            $explosion.classList.add("explosion");

            const $object = document.createElementNS("http://www.w3.org/2000/svg", "foreignObject");
            $object.setAttribute("x", -60);
            $object.setAttribute("y", -120);
            $object.setAttribute("width", 120);
            $object.setAttribute("height", 200);
            $object.append($explosion);

            this.$car.append($object);

            let i = 0;
            const interval = setInterval(() => {
                $explosion.style.backgroundPosition = `-${++i * 120}px 0px`;
            }, 70);

            setTimeout(() => {
                const $fire = document.createElement("img");
                $fire.src = "img/fire.gif";
                $fire.style.width = "64px";
                $fire.style.position = "absolute";
                $fire.style.left = "28px";
                $fire.style.top = "78px";
                $object.prepend($fire);
            }, 6 * 70); // 6 frames of 70ms each

            setTimeout(() => {
                clearInterval(interval);
                $explosion.remove();
            }, 17 * 70); // 17 frames of 70ms each
        }
    }

    class PlayerCar extends Car {
        keyStatus = {};
        startTime;

        constructor(carId = "") {
            super("i 0 i 0", carId);
            this.anchors.pop()
        }

        newCommand(command) {
            const now = Date.now();
            const duration = (now - this.startTime) / 1000;
            this.startTime = now;
            this.maxT += duration;

            this.commands.at(-1)[1] = duration;
            this.addAnchorFromCommand(this.commands.at(-1));
            this.commands.push([command, 0]);
        }

        checkCommand() {
            if (this.exploded || !this.startTime)
                return;
            const prevCommand = this.commands.at(-1)[0];
            const newCommand = this.commandFromKeyStatus();
            if (prevCommand !== newCommand) {
                this.newCommand(newCommand);
            }
        }

        commandFromKeyStatus() {
            const ad = this.keyStatus["s"] ? "d" : this.keyStatus["w"] ? "a" : "";
            if (this.keyStatus["d"]) {
                return ad + "r";
            } else if (this.keyStatus["a"]) {
                return ad + "l";
            } else {
                return ad || "i";
            }
        }

        keyDown(e) {
            if ("wasd".includes(e.key)) {
                this.keyStatus[e.key] = true;
                this.checkCommand();
            }
            if (!this.exploded && e.key === "e") {
                soundManager.play(`beep-${beeps[Math.floor(Math.random() * beeps.length)]}`, "");
            }
        }

        keyUp(e) {
            if ("wasd".includes(e.key)) {
                this.keyStatus[e.key] = false;
                this.checkCommand();
            }
        }
    }

    class IntroHandler {
        text;
        next;
        startTime;
        $balloon;
        index = -1;

        constructor(text, next) {
            this.text = text;
            this.next = next;
            this.startTime = Date.now();
            this.$balloon = document.getElementById("balloon");

            this.reset();
            document.getElementById("maarten").style.right = "calc(50% + 64px)";
            document.getElementById("menno").style.left = "calc(50% + 64px)";

            this.tick();
        }

        destroy() {
            document.getElementById("maarten").style.right = "4000px";
            document.getElementById("menno").style.left = "4000px";
            this.reset();
        }

        reset() {
            this.$balloon.style.opacity = "0";
        }

        tick() {
            this.index++;
            if (this.index >= this.text.length) {
                if (this.next === undefined) {
                    return; // Stay forever on the final screen
                }

                this.destroy();

                // Wait for animation of moving away from the machine to call this.next
                setTimeout(() => {
                    this.next();
                }, 1000);

                return;
            }

            [
                document.querySelector("#maarten .emoji").innerText,
                document.querySelector("#menno .emoji").innerText,
            ] = this.text[this.index];

            const balloonText = this.text[this.index][2];
            setTimeout(() => {
                if (balloonText) {
                    this.$balloon.style.opacity = "1";
                }
                this.$balloon.innerText = balloonText;
            }, Math.max(0, 1000 + this.startTime - Date.now()));
        }

        keyDown(e) {
            if (Date.now() - this.startTime > 1000 && isSingleKey(e) && this.index < this.text.length){
                this.tick();
                e.preventDefault();
            }
        }
    }

    class Controller {
        keyDownHandler;
        keyUpHandler;

        constructor() {
            // Cannot simply pass function reference, because that re-binds `this`
            this.keyDownHandler = e => !e.repeat && this.keyDown(e);
            this.keyUpHandler = e => this.keyUp(e);

            window.addEventListener("keydown", this.keyDownHandler);
            window.addEventListener("keyup", this.keyUpHandler);
        }

        destroy() {
            window.removeEventListener("keydown", this.keyDownHandler);
            window.removeEventListener("keyup", this.keyUpHandler);
        }

        keyDown (e) {}

        keyUp (e) {}
    }


    class IntroController extends Controller {
        introHandler = new IntroHandler([
            ["🙂", "🙂", ""],
            ["🥳️", "😄", "Hi Menno, happy birthday!"],
            ["☺️", "😊️", "I hope you already enjoyed your birthday in real life."],
            ["😏️", "😱", "But I bet you're very eager to receive a present from me…"],
            ["🤩", "🤩", "Wait no longer: I'm taking you on a trip to a virtual arcade!"],
            ["😄", "🙂", "There's this new machine with Simple Vrooming Game, the newest arcade racer."],
            ["😊️", "😍", "As part of my gift, I'll insert a bunch of coins for you."],
            ["😁️", "🤓", "Have fun trying to beat my high scores!"],
        ], () => window.location.hash = "#menu");

        destroy() {
            super.destroy();
            this.introHandler.destroy();
        }

        keyDown(e) {
            this.introHandler.keyDown(e);
        }
    }

    class MenuController extends Controller {
        keyDown(e) {
            if (isSpaceKey(e)) {
                window.location.hash = "#level0";
                e.preventDefault();
            }
        }
    }

    class LevelController extends Controller {
        id;
        introText;
        road;
        aiCars;
        playerCar;
        cars;
        introHandler;
        startTime;
        interval;
        lapTimes;
        lapLengths;
        bestLap;
        pressKeyToContinue;
        victory;
        zoomLevel;

        constructor(id, [introText, road, aiCars]) {
            super();
            this.id = id;
            this.introText = introText;
            this.road = new Road(...road);
            this.aiCars = aiCars;

            Array.from($road.children).forEach(el => el.setAttribute("d", this.road.svgPath));
            $road.children[1].setAttribute("stroke-dasharray", this.road.length / Math.floor(this.road.length / 64) / 2);
            $road.children[3].setAttribute("stroke-dasharray",
                `${this.road.length / Math.floor(this.road.length / 256) * 3 / 8} ${this.road.length / Math.floor(this.road.length / 256) * 5 / 8}`);

            this.init();
        }

        init(restart = false) {
            clearInterval(this.interval);
            this.startTime = undefined;
            this.lapTimes = [0];
            this.lapLengths = [];
            this.bestLap = 9e9;
            this.pressKeyToContinue = false;
            this.victory = 0;
            this.zoomLevel = 0;

            if (restart) {
                this.reset();
                this.cars.forEach(car => car.vroom.stop());
            }

            this.playerCar = new PlayerCar();
            this.cars = [this.playerCar, ...this.aiCars.map(aiCar => new Car(...aiCar))];

            this.cars.forEach(car => car.draw());
            this.setHud(0, 0);
            $map.style.transform = `translate(${640 - this.playerCar.x}px,${360 - this.playerCar.y}px)`;

            const $number = document.getElementById("countdown").children[0];

            const startDriving = async () => {
                $number.classList.add("go");

                this.startTime = Date.now();
                this.playerCar.startTime = this.startTime;
                this.playerCar.checkCommand(); // If the player already has some keys pressed, immediately apply them
                this.interval = setInterval(() => this.tick(), 1000 / 60);

                await wait(900);

                $number.classList.remove("go");
                $number.classList.remove("count");

                musicManager.unpause();
            }

            if (instaStart) {
                $number.classList.add("count");
                startDriving();
            } else {
                const levelMusic = musicManager.current;
                if (!instaStart) {
                    musicManager.play("menu");
                }

                const postIntro = () => {
                    this.introHandler = undefined;

                    musicManager.play(levelMusic);
                    musicManager.pause();

                    wait(1000).then(async () => {
                        $number.classList.add("count");
                        soundManager.play("start", "");
                        for (let i = 3; i >= 1; i--) {
                            $number.classList.add(`n${i}`);
                            await wait(990);
                            $number.classList.remove(`n${i}`);
                        }
                        startDriving();
                    });
                };

                if (restart) {
                    postIntro();
                } else {
                    this.introHandler = new IntroHandler(this.introText, postIntro);
                }
            }
        }

        destroy() {
            super.destroy();
            this.introHandler?.destroy();
            this.stopLevel();
            this.reset();
        }

        reset() {
            Array.from(document.getElementsByTagName("foreignObject")).forEach(el => el.remove());
            $bestLap.innerText = "-:-.-";
            $victory.classList.remove("visible");
            $restart.classList.remove("visible");
        }

        stopLevel() {
            clearInterval(this.interval);
            this.cars.forEach(car => car.vroom.stop());
            musicManager.stop();
        }

        tick() {
            const t = (Date.now() - this.startTime) / 1000;
            if (t < 0) return;

            this.cars.forEach(car => car.step(t, this.road, this.playerCar));
            this.cars.forEach(car => car.draw());

            if (!this.victory && this.playerCar.exploded) {
                this.stopLevel();
                if (this.id === 0) {
                    // Don't immediately go to level1, but wait a second to make sure the player has stopped driving.
                    setTimeout(() => this.pressKeyToContinue = true, 1000);
                } else {
                    $restart.classList.add("visible");
                }
                return;
            }

            const {s, laps} = this.playerCar;

            if (laps > this.lapLengths.length) {
                this.lapTimes.push(t);
                const lapLength = this.lapTimes.at(-1) - this.lapTimes.at(-2);
                this.lapLengths.push(lapLength);
                if (lapLength < this.bestLap) {
                    this.bestLap = lapLength;
                    $bestLap.innerText = printTime(lapLength);
                }

            }

            if (!this.victory && laps === (this.id === 3 ? 1 : 3)) {
                this.victory = t;
                $victory.classList.add("visible");
                document.getElementById("total-time").innerText = printTime(this.victory);
                document.getElementById("laps").innerHTML = this.lapLengths.map(time =>
                    `<span${time === this.bestLap ? ` class="best"` : ""}>${printTime(time)}</span>`).join("");
                musicManager.stop();
                soundManager.play("victory", "");
            }

            this.setHud(this.victory || t, s);
            if (!this.victory) {
                this.setMapTransform();
            }
        }

        setHud(t, s) {
            if (!this.victory && this.lapLengths.length > 0 && t - this.lapTimes.at(-1) < 3) {
                $time.innerText = printTime(this.lapLengths.at(-1));
                $time.style.opacity = (t - this.lapTimes.at(-1)) % 0.5 < 0.35 ? 1 : 0;
            } else {
                $time.innerText = printTime(t);
                $time.style.opacity = 1;
            }
            $speed.children[0].innerText = Math.floor(s);
            Array.from($speedometer.children).forEach((c, i) => c.style.display = s > 64 * (11 - i) ? "" : "none");
        }

        setMapTransform() {
            const {x, y} = this.playerCar;
            const scaleFactor = Math.pow(2, this.zoomLevel / 4);
            $map.style.transform = `scale(${scaleFactor}) translate(${640 / scaleFactor - x}px,${360 / scaleFactor - y}px)`;
        }

        outro() {
            musicManager.play("menu");
            $map.style.transition = "transform 5s ease-in-out";
            this.introHandler = new IntroHandler([
                ["😄", "🥳️", "Wow, you beat the game, congratulations!"],
                ["😇", "☺️", "I wanted to add way more features, but this already took more time than planned."],
                ["🤔", "😱", "Wait... what's this...?"],
            ], async () => {
                this.introHandler = undefined;
                $victory.classList.remove("visible");

                $map.style.transform = "scale(0.08) translate(3200px, 4200px)";
                await wait(8000);

                this.introHandler = new IntroHandler([
                    ["🥳️", "🥳️", "Happy birthday again, and Merry Christmas!"],
                ], undefined);
            });
        }

        keyDown(e) {
            if (this.pressKeyToContinue && isSingleKey(e)) {
                window.location.hash = "#level1";
                return;
            }
            if (this.victory && isSpaceKey(e) && !this.introHandler) {
                if (this.id < 3) {
                    window.location.hash = "#level" + (this.id + 1);
                } else {
                    this.outro();
                }
                return;
            }

            if (isSingleKey(e) && e.key === "r") {
                this.init(true);
                return;
            }
            if (e.key === "0") {
                this.zoomLevel = 0;
                this.setMapTransform();
                return;
            }
            if (e.key === "-") {
                this.zoomLevel--;
                this.setMapTransform();
                return;
            }
            if (e.key === "=") {
                this.zoomLevel++;
                this.setMapTransform();
                return;
            }

            if (this.introHandler) {
                this.introHandler.keyDown(e);
            }
            if (!this.victory) {
                this.playerCar.keyDown(e);
            }
        }

        keyUp(e) {
            if (!this.victory) {
                this.playerCar.keyUp(e);
            }
        }
    }

    class AudioManager {
        id;
        $button;
        on;
        files;

        constructor(id, files) {
            this.id = id;
            this.$button = document.getElementById(id);
            this.on = !!(localStorage.getItem(id) ?? true);
            if (!this.on) {
                this.$button.classList.toggle("off");
            }

            this.$button.addEventListener("click", (e) => {
                this.$button.classList.toggle("off");
                this.on = !this.on;
                localStorage.setItem(id, this.on ? "on" : "");
                if (this.on) {
                    this.resume();
                } else {
                    this.stop();
                }
            });

            this.files = Object.fromEntries(files.map(file => [file, new Audio(`${id}/${file}.mp3`)]));
        }

        resume() {}

        stop() {}
    }

    class MusicManager extends AudioManager {
        current;
        paused = false;

        constructor(...files) {
            super("music", files);
            Object.values(this.files).forEach(file => file.loop = true);
        }

        play(id) {
            if (this.current) {
                this.files[this.current].pause();
            }
            this.current = id;
            setTimeout(() => this.resume(), 42);
        }

        resume() {
            if (this.on && !this.paused) {
                this.files[this.current].play();
            }
        }

        stop() {
            Object.values(this.files).forEach(file => {
                file.pause();
                file.currentTime = 0;
            });
        }

        pause() {
            this.paused = true;
            this.stop();
        }

        unpause() {
            this.paused = false;
            this.resume();
        }
    }

    class SoundManager extends AudioManager {
        instances;

        constructor(...files) {
            super("sound", files);
            this.instances = Object.fromEntries(files.map(file => [file, {}]));
        }

        play(fileId, id, distanceToScreen = 0) {
            if (!this.on) {
                return;
            }
            const audio = this.files[fileId].cloneNode();
            audio.volume = 1.0 / Math.pow(Math.max(1, (distanceToScreen - 360) / 100), 2);
            audio.play();
            this.instances[fileId][id] = audio;
            setTimeout(() => {
                if (this.instances[fileId][id] === audio) {
                    delete this.instances[fileId][id];
                }
            }, audio.duration + 100);
        }

        resume() {
            if (this.on) {
                Object.values(this.instances).forEach(fileInstances =>
                    Object.values(fileInstances).forEach(audio => {
                        if (audio.loop) {
                            audio.currentTime = 0;
                            audio.play();
                        }
                    }));
            }
        }

        stop() {
            Object.values(this.instances).forEach(fileInstances =>
                Object.values(fileInstances).forEach(audio => audio.pause()));
        }
    }

    const musicManager = new MusicManager("intro", "menu", "level0", "level1");
    const soundManager = new SoundManager(
        "start", "victory", "vroom",
        ...beeps.map(i => `beep-${i}`),
        ...[0, 1, 2, 3].map(i => `explosion${i}`),
    );

    class Sound {
        // Perfect looping of sound apparently requires AudioContext: https://stackoverflow.com/a/47052301
        static audioContext = new (AudioContext || webkitAudioContext)();

        file;
        id;
        audioElement;

        constructor(file, id) {
            this.file = file;
            this.id = id;
            const audioNode = Sound.audioContext.createMediaElementSource(soundManager.files[file].cloneNode());
            audioNode.connect(Sound.audioContext.destination);
            this.audioElement = audioNode.mediaElement;
            this.audioElement.loop = true;
            this.audioElement.preservesPitch = false;
        }

        play() {
            soundManager.instances[this.file][this.id] = this.audioElement;
            if (soundManager.on) {
                this.audioElement.play();
            }
        }

        setVolume(distanceToScreen, dopplerFactor) {
            this.audioElement.volume = Math.min(1.0, Math.max(0.0,
                dopplerFactor / Math.pow(Math.max(1, (distanceToScreen - 360) / 100), 2)));
        }

        stop() {
            this.audioElement.pause();
            delete soundManager.instances[this.file][this.id];
        }
    }

    let tmp;
    const levels = [
        [
            [
                ["😄", "😄", "Oohh, the game is about to start!"],
                ["🙂", "🙂", "To control your car, press W to accelerate and S to brake."],
                ["☺️", "🙂", "Note that the game does not simulate friction, so you'll keep your speed."],
                ["🤖", "😱", "You'll also see half-transparent cars, which are pre-programmed opponents."],
                ["🤓", "🤓", "Ready? Set... Go!"],
            ],
            [128, "i 18"],
            [["a 1", 1]],
        ],
        [
            [
                ["🙈", "😵‍💫", "Woops, looks like you crashed, but what could you have done..."],
                ["😛", "😅", "I guess that was just the first part of the tutorial."],
                ["🙂", "🙂", "Next lesson: pressing A or D makes your car turn left or right."],
                ["😊️", "🙂", "The faster you go, the wider your turns. So you've got to slow down for sharp curves."],
                ["🤓", "🤓", "Ready? Set... Go!"],
            ],
            [512, ` i 2 r ${quartPi}`.repeat(4)],
            [["a 4" + ` r ${quartPi} i 2`.repeat(4), 1]],
        ],
        [
            [
                ["😄", "🥳️", "Well done, nice lap times!"],
                ["🙂", "😯", "The next level will be a bit more curvy."],
                ["🤓", "🤓", "Ready? Set... Go!"],
            ],
            [400, ` i 1 r 0.3 l 0.6 r 0.3 i 1 r ${halfPi}`.repeat(2)],
            [["a 3 i 1.8" + ` r ${halfPi} i 3.4`.repeat(4), 1]],
        ],
        [
            [
                ["😒", "😅", "Oh. Your opponent just drove straight ahead..."],
                ["😄", "😱", "I hope that's enough practice for the final level, this will be a big one!"],
                ["🤓", "🤓", "Ready? Set... Go!"],
            ],
            [768, tmp = `d 2 ${" " || "/* starting speed is actually 512 */"}
                l ${quartPi} i 1 d 2 r ${halfPi} a 1 d 1 l ${quartPi} i 1 l ${quartPi} a 1 d 1 r ${halfPi} a 2 i 1 l ${quartPi}
                l ${pi * 0.2} i 2.7 r ${pi * 0.4} i 2.7 l ${pi * 0.2}
                l ${quartPi} i 2.25 r ${quartPi} a 1 r ${halfPi} d 0.5 l ${quartPi} d 0.5 i 0.4125 l ${quartPi} i 1.5
                l ${quartPi} i 2.25 r ${quartPi} a 1 r ${halfPi} d 0.5 l ${quartPi} d 0.5 i 0.4125 l ${quartPi} i 1.5
                i 0.5 l ${pi / 6} i 1 l ${pi / 6} d 2 r ${halfPi} a 1 i 0.3 l ${pi / 3} i 0.3 d 1 r ${halfPi} a 2 i 1 l ${pi / 3} i 2
                                                      r ${pi / 6} i 0.9 d 1 l ${pi / 3} i 0.3 d 1 r ${halfPi} a 2 i 1.5 r ${pi / 6}
                r ${pi * 0.2} i 2.7 l ${pi * 0.4} i 2.7 r ${pi * 0.2}
                r ${quartPi} i 0.75 ${` i 0.5 l ${pi / 16}`.repeat(4)} l ${quartPi} i 2.25 r ${quartPi}
                r ${quartPi} i 1 d 2 l ${halfPi} a 1 d 1 r ${quartPi} i 1 r ${quartPi} a 1 d 1 l ${quartPi} ${" " || "/* missing a 2, next speed is 256 */"}
                a 2 d 2 l ${halfPi} a 0.75 d 0.75 r ${quartPi} a 2 i 0.5 r ${quartPi} i 1.5
                r ${pi / 6} i 0.5 d 1 r ${pi / 3} a 0.5 l ${quartPi} i 0.4 l ${quartPi} a 0.5 i 0.2 l ${quartPi} i 2.25 r ${quartPi}
                d 1 r ${quartPi} a 1.1 d 2.1 l ${halfPi} a 2.1 d 1.1 r ${quartPi} a 1
                r ${quartPi} i 0.25 l ${quartPi} i 0.25 r ${halfPi} l ${halfPi} i 0.5 l ${quartPi} i 2.25 r ${quartPi}
                a 2 r ${quartPi} i 1.78435 r ${quartPi} i 1.90692 ${" " || "/* Last two commands have been brute-forced 😇 */"}
            `],
            [["a 4 i 0.5 " + tmp.substring(4) + tmp.repeat(3), 1]],
        ],
    ];

    const monitorOn = () => {
        // Monitor starts with "opacity: 0" so that the turn-off animation doesn't show on page load
        $monitor.style.opacity = "";
        $monitor.classList.add("on");
    };

    const monitorOff = () => {
        $monitor.classList.remove("on");
    };

    let controller = new Controller();

    const processHash = (first) => {
        document.body.scrollTop = 0; // Prevent <body> from scrolling down on hash change

        let controllerFactory;
        let $element;
        let newMonitorState = false;

        const hash = window.location.hash.substring(1);
        if (hash === "menu") {
            controllerFactory = () => new MenuController();
            $element = $menu;
            newMonitorState = true;
            musicManager.play("menu");
        } else if (hash.startsWith("level")) {
            const level = parseInt(hash.substring(5));
            controllerFactory = () => new LevelController(level, levels[level]);
            $element = $field;
            newMonitorState = true;
            musicManager.play(`level${level % 2}`);
        } else {
            controllerFactory = () => new IntroController();
            musicManager.play("intro");
        }

        const switchController = () => {
            controller.destroy();

            [$menu, $field].forEach(el => el.style.display = "none");
            if ($element) {
                $element.style.display = "";
            }

            controller = controllerFactory();
        };

        if ($monitor.classList.contains("on") != newMonitorState) {
            if (newMonitorState) {
                if (first)
                    setTimeout(monitorOn, 750);
                else
                    monitorOn();
                switchController();
            } else {
                monitorOff();
                setTimeout(switchController, 600);
            }
        } else {
            switchController();
        }
    };

    window.addEventListener("hashchange", () => processHash());
    processHash(!instaStart);
    if (instaStart) $monitor.classList.add("instaStart");
};

// Chrome does not play auto-playing sound before the user interacted with the page.
// To avoid hacks all over the place, just delay loading until the user interacts.
const loader = (e) => {
    ["click", "keydown"].forEach(eventType =>
        window.removeEventListener(eventType, loader));
    document.getElementById("pre-load").remove();
    load();
};

window.onload = () =>
    ["click", "keydown"].forEach(eventType =>
        window.addEventListener(eventType, loader));
