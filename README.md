# Menno Birthday Game 2023

Happy Birthday and/or Merry Christmas, Menno! 🥳

This game takes pride in being written in pure HTML, CSS, ES6, and Vim.

For an impression of the making-of, see the [progress](/progress/#progress-screenshots) folder.

## Credits

- Game authored by Maarten Sijm,
  released under CC BY-SA 4.0
- Graphic design: Maarten Sijm and Davina van Meer
- Sound design: Maarten Sijm, Davina van Meer, and the other Vriendjes
- Photo of Maarten modified from
  [Digital Division](https://www.digitaldivision.so/)
  (private communication)
- Photo of Menno modified from
  [Level Level](https://level-level.com/nl/team/menno-van-den-ende/)
- Cars modified from qubodup from OpenClipart,
  released under CC0 1.0
  on [FreeSvg](https://freesvg.org/bright-green-racing-car-vector-illustration)
  - Logos used:
    [wolf](https://freesvg.org/wolf-silhouette-vector-image),
    [dragon](https://freesvg.org/racing-automobile)
- Explosion GIF modified from
  [Tenor](https://tenor.com/view/explosion-boom-gif-13902355)
- Fire GIF from Mojang Studios,
  released with all rights reserved by Mojang and Microsoft
  on [minecraft.fandom.com](https://minecraft.fandom.com/wiki/Fire?file=Fire.gif)
- Arcade background modified from Matt Cole,
  released under Vecteezy Free License
  on [Vecteezy](https://www.vecteezy.com/vector-art/6158500-retro-arcade-cabinet-isolated-on-white-background)
- Speedometer graphics modified from Olivia Timplaru,
  released under Vecteezy Free License
  on [Vecteezy](https://www.vecteezy.com/vector-art/20711508-speedometer-vector-design-illustration-isolated-on-white-background)
- Digital Dismay font by Chequered Ink,
  released under 1001Fonts general font usage terms
  on [1001fonts](https://www.1001fonts.com/digital-dismay-font.html)
- Noto Color Emoji font by Google,
  released under SIL Open Font License v1.1
  on [GitHub](https://github.com/googlefonts/noto-emoji)
- Pixeloid font by GGBotNet,
  released under SIL Open Font License
  on [FontSpace](https://www.fontspace.com/pixeloid-font-f69232)
- Intro music: Wallpaper by Kevin MacLeod,
  released under CC BY 4.0
  on [Incompetech](https://incompetech.com/music/royalty-free/music.html)
- Menu music: Itty Bitty 8 Bit by Kevin MacLeod,
  released under CC BY 4.0
  on [Incompetech](https://incompetech.com/music/royalty-free/music.html)
- Level music: Pixel Peeker Polka - faster by Kevin MacLeod,
  released under CC BY 4.0
  on [Incompetech](https://incompetech.com/music/royalty-free/music.html)
- Level music: Pookatori and Friends by Kevin MacLeod,
  released under CC BY 4.0
  on [Incompetech](https://incompetech.com/music/royalty-free/music.html)
- Race Get Ready sound modified from ZapSplat,
  released under ZapSplat Standerd Licence
  on [ZapSplat](https://www.zapsplat.com/music/retro-car-racing-video-game-get-ready-go-beep-tone/)
- Victory sound from ZapSplat,
  released under ZapSplat Standerd Licence
  on [ZapSplat](https://www.zapsplat.com/music/game-sound-trumpet-fanfare-success/)
- Rally Car Idle Loop 17 by Pixabay,
  released under Pixabay Content License
  on [Pixabay](https://pixabay.com/sound-effects/rally-car-idle-loop-17-84405/)
- Explosion sounds by Mojang Studios,
  released with all rights reserved by Mojang and Microsoft
  on [minecraft.fandom.com](https://minecraft.fandom.com/wiki/TNT#Unique)

NOT AN OFFICIAL MINECRAFT GAME. NOT APPROVED BY OR ASSOCIATED WITH MOJANG OR MICROSOFT.
